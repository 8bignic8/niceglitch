from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context
import st3m.run
import time
import random

class ThirdScreen(BaseView):
    def __init__(self) -> None:
        super().__init__()
        self._squares = []
    
    def draw(self, ctx: Context) -> None:
        
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        # Green square
        while (len(self._squares) < 100):
            rand = random.random()+.001
            r = 0.9*rand
            g = rand*.5
            b = 0.01
            side_length = int(80 * random.random())
            x = int((random.random()-.5)*240)
            y = int((random.random()-.5)*240)
            ctx.rgb(r,g,b).rectangle(x,y, side_length, side_length).fill()
            self._squares.append((r, g, b, side_length, x, y))
        
        for square in self._squares:
           ctx.save()
           ctx.rgb(square[0],square[1],square[2]).rectangle(square[4], square[5], square[3], square[3]).fill()
           ctx.restore()

        del self._squares[0:2]

class SecondScreen(BaseView):
    def __init__(self) -> None:
        super().__init__()
        self._squares = []
    
    def draw(self, ctx: Context) -> None:
        
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        # Green square
        while (len(self._squares) < 100):
            r = random.random()
            g = random.random()
            b = random.random()
            side_length = int(80 * random.random())
            x = int((random.random()-.5)*240)
            y = int((random.random()-.5)*240)
            ctx.rgb(r,g,b).rectangle(x,y, side_length, side_length).fill()
            self._squares.append((r, g, b, side_length, x, y))
        
        for square in self._squares:
           ctx.save()
           ctx.rgb(square[0],square[1],square[2]).rectangle(square[4], square[5], square[3], square[3]).fill()
           ctx.restore()

        del self._squares[0:2]

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        if self.input.buttons.app.left.pressed:
            self.vm.push(ThirdScreen())
    

class glitchIngSquares(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._x = -20.
        # Ignore the app_ctx for now.

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        #ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        # Red square
        #ctx.rgb(255, 0, 0).rectangle(-20, -20, 40, 40).fill()

        count = 0
        ctx.rgb(0,0, 0).rectangle(-120, -120, 240, 240).fill()
        while (count<15):
            r = random.random()
            g = random.random()
            b = random.random()
            side_length = int(80 * random.random())
            ctx.rgb(r,g,b).rectangle(int((random.random()-.5)*240), int((random.random()-.5)*240), side_length, side_length).fill()
            count += 1

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        direction = ins.buttons.app # -1 (left), 1 (right), or 2 (pressed)

        if direction == ins.buttons.PRESSED_LEFT:
            self._x -= 20 * delta_ms / 1000
        elif direction == ins.buttons.PRESSED_RIGHT:
            self._x += 40 * delta_ms / 1000

        if self.input.buttons.app.left.pressed:
            self.vm.push(SecondScreen())


if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(glitchIngSquares(ApplicationContext()))